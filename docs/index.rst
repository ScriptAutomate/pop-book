.. pop-book documentation master file, created by
   sphinx-quickstart on Thu Apr  2 09:52:44 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Intro to Plugin Oriented Programming
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   main/problem
   main/origin
   main/plugable
   main/seed
   main/hub
   main/subs
   main/patterns
   main/app_merging
   main/plugins
   main/instances
   main/conf
   main/contracts
   main/build
   main/conclusion



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
