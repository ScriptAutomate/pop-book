=================================
Using Plugin Oriented Programming
=================================

Plugin Oriented Programming has been designed for general use and to
make it easy to create many types of applications. App merging allows for
new levels of compartmentalization and a new generation of code-reuse.
App Merging also allows developers to find greater freedom from maintenance
woes while still expressing powerful software.

By drawing inspiration from a rich history of computing, Plugin Oriented Programming
presents high level design patterns that have been created to make development
easy for developers. The focus on development that is easy to get lots of
work done quickly from scratch while presenting a paradigm that scales creates
a deeply unique opportunity for developers.

Plugin Oriented Programming is, as of this writing, very young. But it is
being used in production environments today. It has grown up in large scale
deployments and under the stress of large scale projects.

